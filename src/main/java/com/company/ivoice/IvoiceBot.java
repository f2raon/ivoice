package com.company.ivoice;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import org.jetbrains.annotations.NotNull;

public class IvoiceBot extends AbstractBehavior<Ivoice.Answer> {

    public static Behavior<Ivoice.Answer> create() {
        return Behaviors.setup(context -> new IvoiceBot(context));
    }

    private IvoiceBot(ActorContext<Ivoice.Answer> context) {
        super(context);
    }

    @Override
    public Receive<Ivoice.Answer> createReceive() {
        return newReceiveBuilder().onMessage(Ivoice.Answer.class, this::onAnswered).build();
    }

    private Behavior<Ivoice.Answer> onAnswered(@NotNull Ivoice.Answer answer) {
        answer.question.tell(new Ivoice.Question(answer.answer, getContext().getSelf()));
        return Behaviors.stopped();
    }
}