package com.company.ivoice;

import akka.actor.typed.ActorSystem;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        run("please, ask your question", true);
    }

    public static void run(String text,boolean firstQuestion) {
        final ActorSystem<IvoiceMain.AskQuestion> ivoiceMain = ActorSystem.create(IvoiceMain.create(), "faq");

        System.out.println(text);
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        if (!firstQuestion && name.toLowerCase().equals("n")) {
            ivoiceMain.terminate();
            System.exit(0);
        } else {
            ivoiceMain.tell(new IvoiceMain.AskQuestion(name));
            run("any question ? Type \"N\" for termination", false);
        }
    }
}
