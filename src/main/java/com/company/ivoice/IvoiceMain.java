package com.company.ivoice;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class IvoiceMain extends AbstractBehavior<IvoiceMain.AskQuestion> {

    public static class AskQuestion {
        public final String question;

        @Contract(pure = true)
        public AskQuestion(String question) {
            this.question = question;
        }
    }

    private final ActorRef<Ivoice.Question> question;

    public static Behavior<AskQuestion> create() {
        return Behaviors.setup(IvoiceMain::new);
    }

    private IvoiceMain(ActorContext<AskQuestion> context) {
        super(context);
        question = context.spawn(Ivoice.create(), "faq");
    }

    @Override
    public Receive<AskQuestion> createReceive() {
        return newReceiveBuilder().onMessage(AskQuestion.class, this::onAsk).build();
    }

    @Contract("_ -> this")
    private Behavior<AskQuestion> onAsk(@NotNull AskQuestion model) {
        ActorRef<Ivoice.Answer> answer = getContext().spawn(IvoiceBot.create(), model.question);
        question.tell(new Ivoice.Question(model.question, answer));
        return this;
    }
}
