package com.company.ivoice;

import akka.actor.ActorSystem;
import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import akka.dispatch.ExecutionContexts;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.*;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Objects;

public class Ivoice extends AbstractBehavior<Ivoice.Question> {

    public static final class Question {
        public final String question;
        public final ActorRef<Answer> answer;

        @Contract(pure = true)
        public Question(String question, ActorRef<Answer> answer) {
            this.question = question;
            this.answer = answer;
        }
    }

    public static final class Answer {
        public final String answer;
        public final ActorRef<Question> question;

        @org.jetbrains.annotations.Contract(pure = true)
        public Answer(String answer, ActorRef<Question> question) {
            this.answer = answer;
            this.question = question;
        }

        @Contract(value = "null -> false", pure = true)
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Answer answer = (Answer) o;
            return Objects.equals(answer, answer.answer) &&
                    Objects.equals(question, answer.question);
        }

        @Override
        public int hashCode() {
            return Objects.hash(answer, question);
        }

        @NotNull
        @Contract(pure = true)
        @Override
        public String toString() {
            return "faq{" +
                    "question='" + question + '\'' +
                    ", answer=" + answer +
                    '}';
        }

    }

    public static Behavior<Question> create() {
        return Behaviors.setup(Ivoice::new);
    }

    private Ivoice(ActorContext<Question> context) {
        super(context);
    }

    @Override
    public Receive<Question> createReceive() {
        return newReceiveBuilder().onMessage(Question.class, this::onAsk).build();
    }

    @Contract("_ -> this")
    private Behavior<Question> onAsk(@NotNull Question question) throws IOException {

        String url = "https://odqa.demos.ivoice.online/model";
        String data = String.format("{\"context\": [\"%s\"]}", question.question);

        System.out.println(String.format("Question: %s!", question.question));

        ActorSystem system = ActorSystem.create();
        Materializer materializer = ActorMaterializer.create(system);

        Http.get(system).
                singleRequest(HttpRequest.create(url).withEntity(data), materializer).
                thenCompose(response -> Unmarshaller.entityToString().unmarshal(response.entity(), ExecutionContexts.global(), materializer)).
                thenAccept(System.out::println);

//        RequestEntity response = HttpRequest.POST(url)
//                .withEntity(HttpEntities.create(ContentTypes.APPLICATION_JSON, data)).entity();
//
//        System.out.println(response.toString());


        System.out.println("=============");

        return this;
    }

}